#include <partAcceptSetup.h>

partAcceptSetup::partAcceptSetup()
{
    m_qHBLytAccept = new QHBoxLayout;
    m_qPushBHelp = new QPushButton("Help");
    m_qPushBOK = new QPushButton("OK");     
    m_qPushBCancel = new QPushButton("Cancel");
    QSpacerItem *qSpacerAccept = new QSpacerItem(1000, 0, QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_qPushBHelp->setEnabled(false);
    m_qHBLytAccept->addWidget(m_qPushBHelp);
    m_qHBLytAccept->addItem(qSpacerAccept);
    m_qHBLytAccept->addWidget(m_qPushBOK);
    m_qHBLytAccept->addWidget(m_qPushBCancel);

}
