#ifndef __ALLOCMEM_BOX4_
#define __ALLOCMEM_BOX4_

#include <QGridLayout>
#include <QCheckBox>
#include <QGroupBox>

class allocateMemBox4:public QWidget{
    Q_OBJECT
    public:
        QGroupBox *m_qGBoxAllocMem4;
        QCheckBox **m_qCheckBox4;        
        QGridLayout *m_qGLytChkBox4;
        bool* m_arrChekedChBox4;
        allocateMemBox4();
        
};
#endif


