#ifndef __ENABLEDAPB_BOX5_
#define __ENABLEDAPB_BOX5_

#include <QGridLayout>
#include <QCheckBox>
#include <QGroupBox>

class enablAPBBox5:public QWidget{
    Q_OBJECT
    public:
        QGroupBox *m_qGBoxEnabledAPB5;
        QCheckBox **m_qCheckBox5;        
        bool* m_arrChekedChBox5;
        QGridLayout *m_qGLytChkBox5;
        enablAPBBox5();
};
#endif


