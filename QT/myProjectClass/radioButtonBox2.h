#ifndef __RADIO_BOX_
#define __RADIO_BOX_

#include <QHBoxLayout>
#include <QString>
#include <QLabel>
#include <QGroupBox>
#include <QRadioButton>

class radioButtonBox2:public QWidget{
    Q_OBJECT
    protected:
        QLabel *m_qLblDataWidth;
    public: 
        QStringList m_qStrLstForRadBut;
        QRadioButton ** m_qRadioB; 
        QGroupBox * m_qGBoxDataWidth;
        QHBoxLayout * m_qHBlytGBoxDataWidth; 
        radioButtonBox2(const QString&, const QString&, const QStringList&, int, int);
};

#endif

