#ifndef __LABEL_COMBOBOX_
#define __LABEL_COMBOBOX_

#include <QVBoxLayout>
#include <QString>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>

class labelComboBox:public QWidget{
    Q_OBJECT
    public: 
        QGroupBox *m_qGBox; 
        QComboBox *m_qComboBox;
        QHBoxLayout *m_qHBLyt;
        labelComboBox();
        labelComboBox(const QString&, const QStringList&, int, QString qStrForGName = "");
};

#endif

