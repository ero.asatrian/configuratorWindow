#include <string>
#include <QtWidgets>
#include "configMainWindow.h"
#include <iostream>

//this class constructor witch recevies that parametrs 
configMainWindow::configMainWindow(QWidget* widgetTab, QTabWidget* tabWidget,
QVBoxLayout* vBoxLayoutMain, QVBoxLayout* vBoxLayoutTab1, 
QString qStringMainWindowName, int width, int height)
{
    m_widgetCentral = new QWidget;
    m_tabWidget = tabWidget;
    m_widgetTab1 = widgetTab; 
    m_VLayoutMain = vBoxLayoutMain;
    m_VLayouTab1 = vBoxLayoutTab1;
    configMainWindow::setupWindowNameSize(qStringMainWindowName, width, height);
}
//This function setup main window name and size 
void configMainWindow::setupWindowNameSize(QString qStringWinName, int width, int
height)
{
    setWindowTitle(qStringWinName);
    setGeometry(0, 0, 100, 100);
    setMinimumSize(width, height);
    configMainWindow::setupTabInWindow("Configuration");
}
//This function setup tab in window
void configMainWindow::setupTabInWindow(const QString tabName)
{
    setCentralWidget(m_widgetCentral);
    m_widgetCentral->setLayout(m_VLayoutMain);
	m_widgetCentral->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);  //Expanding
    m_tabWidget->addTab(m_widgetTab1, tabName);
    m_VLayouTab1->setContentsMargins(10,10,10,120);
}

void configMainWindow::addHeadTitle()
{
    QString qStrForTitle1 = "CoreAPB Configurator";
    QString qStrForTitle2 = "Microchip:PfsFoundation:CoreAPB3:3.5.0.102"; 
    m_headTitle = new headTitle(qStrForTitle1, qStrForTitle2);
    m_VLayoutMain->addWidget(m_headTitle);
    m_headTitle->setAutoFillBackground(true);
}

void configMainWindow::addTabWidget()
{
    m_widgetTab1 = new QWidget(m_tabWidget);
    m_VLayouTab1 = new QVBoxLayout(m_widgetTab1);
    m_VLayoutMain->addWidget(m_tabWidget);
}

void configMainWindow::addGlobConfigGBox1()
{
    QString qStrLblGBox1 = "Reset Type";
    QStringList qStrLstForGlobConf = {"Asynchronous", "Synchronous"}; 
    QString qStrForLblG1 = "Reset Type";
    m_labelWidthComboBox = new labelComboBox(qStrForLblG1, qStrLstForGlobConf, 1700, "Global Configuration");
    QString test = "Synchronous";
    m_labelWidthComboBox->m_qComboBox->setCurrentText(readJSONQStr(m_fileName, qStrLblGBox1));
    
    m_VLayouTab1->addWidget(m_labelWidthComboBox->m_qGBox);
}

void configMainWindow::addDataConfigGBox2()
{
    int countButton = 3;
    QString qStrForGBox = "Data Width Configuration";
    QString qStrForLabel = "APB Master Data Busi Width";
    QStringList qStrForRadBut = {"8-bit", "16-bit", "32-bit"};
    m_radioButtonLblB2 = new radioButtonBox2(qStrForGBox, qStrForLabel, qStrForRadBut, countButton, 900); 
    QString nameChRadBut = readJSONQStr(m_fileName, qStrForLabel);


    for(int i = 0; i < countButton; ++i){
        if(m_radioButtonLblB2->m_qRadioB[i]->text() == nameChRadBut)
    m_radioButtonLblB2->m_qRadioB[i]->setChecked(true); 
    }



    m_VLayouTab1->addWidget(m_radioButtonLblB2->m_qGBoxDataWidth);
}

void configMainWindow::addAddresConfigGBox3()
{
    QString qStrLblGBox3_1 = "Number of address bits driven by master:";
    QString qStrLblGBox3_2 = "Position in slave address of upper 4 bits of master address:";
    QString qStrLblGBox3_3 = "Indirect Addressing";

    m_labelComboBox3 = new labelComboBox3();
    m_labelComboBox3->m_labelWidthComboBox3_1->m_qComboBox->setCurrentText(readJSONQStr(m_fileName, qStrLblGBox3_1));
    m_labelComboBox3->m_labelWidthComboBox3_2->m_qComboBox->setCurrentText(readJSONQStr(m_fileName, qStrLblGBox3_2));
    m_labelComboBox3->m_labelWidthComboBox3_3->m_qComboBox->setCurrentText(readJSONQStr(m_fileName, qStrLblGBox3_3));
    m_VLayouTab1->addWidget(m_labelComboBox3->m_qGBoxAddresConfig);
}

void configMainWindow::addAllocMemGBox4()
{
    m_allocMemBox4 = new allocateMemBox4();
    QString qStrGBoxName = "Allocate memery space to combined region slave";
    readJSONInt(m_fileName, qStrGBoxName, m_allocMemBox4->m_arrChekedChBox4, 16);
    for(int index = 0; index < 16; ++index){
    m_allocMemBox4->m_qCheckBox4[index]->setChecked(m_allocMemBox4->m_arrChekedChBox4[index]);
    }
    m_VLayouTab1->addWidget(m_allocMemBox4->m_qGBoxAllocMem4);
}

void configMainWindow::addEnabledAPBGBox5()
{

    m_enablAPBBox5 = new enablAPBBox5();
    QString qStrGBoxName = "Enabled APB Slave Slots";
    readJSONInt(m_fileName, qStrGBoxName, m_enablAPBBox5->m_arrChekedChBox5, 16);
    for(int index = 0; index < 16; ++index){
    
    m_enablAPBBox5->m_qCheckBox5[index]->setEnabled(!(m_allocMemBox4->m_arrChekedChBox4[index]));
    m_enablAPBBox5->m_qCheckBox5[index]->setChecked(m_enablAPBBox5->m_arrChekedChBox5[index]);
    
}
    m_VLayouTab1->addWidget(m_enablAPBBox5->m_qGBoxEnabledAPB5);
}

void configMainWindow::addTestBanch()
{
    QStringList qStrLstTstBanch = {"User"}; 
    QString qStrForTstBnch = "Testbanch";
    m_testBanch = new labelComboBox(qStrForTstBnch, qStrLstTstBanch, 1700);
    m_VLayouTab1->addLayout(m_testBanch->m_qHBLyt); 
}

void configMainWindow::addPartAccept()
{
    m_partAccept = new partAcceptSetup();
    m_VLayoutMain->addLayout(m_partAccept->m_qHBLytAccept);
    setupWindowNameSize("Configurator", 850, 840);
}

void configMainWindow::saveJsonQGBox1()
{
    qJsonObj["Reset Type"] = m_labelWidthComboBox->m_qComboBox->currentText();
    qDebug() << "GBox1 data sucsesfull saved JSON";
}

void configMainWindow::saveJsonQGBox2()
{
    int countButton = m_radioButtonLblB2->m_qStrLstForRadBut.size();
    for(int i = 0; i < countButton; ++i){
        if(m_radioButtonLblB2->m_qRadioB[i]->isChecked()){
            qJsonObj["APB Master Data Busi Width"] = m_radioButtonLblB2->m_qStrLstForRadBut[i];
        }
       }
    if(qJsonObj["APB Master Data Busi Width"] == ""){
            qJsonObj["APB Master Data Busi Width"] = "None";
    }
    qDebug() << "GBox2 data sucsesfull saved JSON";
}

void configMainWindow::saveJsonQGBox3()
{
    qJsonObj["Number of address bits driven by master:"] = m_labelComboBox3->m_labelWidthComboBox3_1->m_qComboBox->currentText();
    qJsonObj["Position in slave address of upper 4 bits of master address:"] = m_labelComboBox3->m_labelWidthComboBox3_2->m_qComboBox->currentText();
    qJsonObj["Indirect Addressing"] = m_labelComboBox3->m_labelWidthComboBox3_3->m_qComboBox->currentText();
    qDebug() << "GBox3 data sucsesfull saved JSON";
}

void configMainWindow::saveJsonQGBox4()
{
    QJsonArray qJsonArrForGBox4;
    int countButton = 16;
    for(int i = 0; i < countButton; ++i){
        if(m_allocMemBox4->m_qCheckBox4[i]->isChecked()){
            qJsonArrForGBox4.append(i);
        }
    }
    if (qJsonArrForGBox4[0] == ""){
        qJsonArrForGBox4.append("None");
    }
    qJsonObj.insert("Allocate memery space to combined region slave", qJsonArrForGBox4);

    qDebug() << "GBox4 data sucsesfull saved JSON";
    
}

void configMainWindow::saveJsonQGBox5()
{
    QJsonArray qJsonArrForGBox5;
    int countButton = 16;
    for(int i = 0; i < countButton; ++i){
        if(m_enablAPBBox5->m_qCheckBox5[i]->isChecked()){
            qJsonArrForGBox5.append(i);
        }
    }
    if (qJsonArrForGBox5[0] == ""){
        qJsonArrForGBox5.append("None");
    }
    qJsonObj.insert("Enabled APB Slave Slots", qJsonArrForGBox5);
    qDebug() << "GBox5 data sucsesfull saved JSON";
}

void configMainWindow::saveJsonTestBanch()
{
    qJsonObj["Testbanch"] = m_testBanch->m_qComboBox->currentText();
    QJsonDocument qJsonDoc(qJsonObj);
    QFile file("jsonConfigurator.json");
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Could not open file for writing";
        return;
    }
    file.write(qJsonDoc.toJson());
    file.close();
    qDebug() << "TestBanch data sucsesfull saved JSON";
}


QString configMainWindow::readJSONQStr(const QString& fileName,const QString& keyJSON)
{
    QFile fileJson(fileName);
    if (!fileJson.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Failed to open" << fileName  << "for reading";
    }
    QByteArray jsonData = fileJson.readAll();
    fileJson.close();

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);

    if (parseError.error != QJsonParseError::NoError) {
        qDebug() << "Failed to parse JSON:" << parseError.errorString();
        return "1";
    }
    if (jsonDoc.isObject()) {
        QJsonObject jsonObject = jsonDoc.object();
        QString qStrLabel = jsonObject[keyJSON].toString();
        return qStrLabel;
    }
}

bool* configMainWindow::readJSONInt(const QString& fileName, const QString& keyJSON, bool* arrChekedChBox, int size)
{
    for(int i = 0; i < size; ++i){
        arrChekedChBox[i] = 0;
    }
    int chekNum = -1;
    QFile fileJson(fileName);
    if (!fileJson.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Failed to open" << fileName  << "for reading";
    }
    QByteArray jsonData = fileJson.readAll();
    fileJson.close();

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);

    if (parseError.error != QJsonParseError::NoError) {
        qDebug() << "Failed to parse JSON:" << parseError.errorString();
    }
    if (jsonDoc.isObject()) {
        QJsonObject jsonObject = jsonDoc.object();
        if(jsonObject.contains(keyJSON) && jsonObject[keyJSON].isArray()){
        QJsonArray checkNumArray = jsonObject[keyJSON].toArray(); 
        for(const QJsonValue& number: checkNumArray){
            if(number.isDouble()){
                chekNum = number.toInt();
                arrChekedChBox[chekNum] = 1;
            }
        }
        } else {
            qDebug() << "JSON does not contain" << keyJSON << "array";
        }
        return arrChekedChBox;
    }

}


//This is class constructor witch in the main window puts GBox
configMainWindow::configMainWindow()
{
    m_widgetCentral = new QWidget;
    m_VLayoutMain = new QVBoxLayout(m_widgetCentral);
    m_tabWidget = new QTabWidget(m_widgetCentral);
    m_fileName = "jsonConfigurator.json";
    QString qStrLblGBox2 = "APB Master Data Busi Width";
    QString qStrLblTestBanch = "User";


    addHeadTitle();
    addTabWidget(); 
    addGlobConfigGBox1();
    addDataConfigGBox2();
    addAddresConfigGBox3();
    addAllocMemGBox4();
    addEnabledAPBGBox5();
    addTestBanch();
    addPartAccept();
    readJSONQStr(m_fileName, qStrLblGBox2);
    readJSONQStr(m_fileName, qStrLblTestBanch);

    for(int i = 0; i < 16; ++i){    
        QObject::connect(m_allocMemBox4->m_qCheckBox4[i], &QCheckBox::stateChanged, m_enablAPBBox5->m_qCheckBox5[i], &QCheckBox::setDisabled);
}
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonQGBox1);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonQGBox2);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonQGBox3);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonQGBox4);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonQGBox5);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::saveJsonTestBanch);
    QObject::connect(m_partAccept->m_qPushBOK, &QPushButton::clicked, this, &configMainWindow::close);
}

