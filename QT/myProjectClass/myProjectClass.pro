######################################################################
# Automatically generated by qmake (3.1) Mon Dec 18 19:50:08 2023
######################################################################

TEMPLATE = app
TARGET = myProjectClass
INCLUDEPATH += .
QT += widgets

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += allocateMemBox4.h \
           configMainWindow.h \
           enablAPBBox5.h \
           headTitle.h \
           labelComboBox.h \
           labelComboBox3.h \
           partAcceptSetup.h \
           radioButtonBox2.h
SOURCES += allocateMemBox4.cpp \
           configMainWindow.cpp \
           enablAPBBox5.cpp \
           headTitle.cpp \
           labelComboBox.cpp \
           labelComboBox3.cpp \
           main.cpp \
           partAcceptSetup.cpp \
           radioButtonBox2.cpp
