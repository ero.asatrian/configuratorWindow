#include "allocateMemBox4.h"

allocateMemBox4::allocateMemBox4()
{

    int arrSize = 16;
    m_arrChekedChBox4 = new bool[arrSize]; 
    QString qStrGBoxName = "Allocate memery space to combined region slave";
    QSpacerItem *slaveSpacer = new QSpacerItem(1000, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_qGBoxAllocMem4 = new QGroupBox(qStrGBoxName);
    m_qGLytChkBox4 = new QGridLayout;
    m_qCheckBox4 = new QCheckBox*[arrSize];
    for (int row = 0; row < 4; ++row){
        for (int col = 0; col < 4; ++col){
            int index = row * 4 + col;
            m_qCheckBox4[index] = new QCheckBox(QString("Slot %1").arg(index));
            if (index < 10){
                m_qCheckBox4[index]->setStyleSheet("QCheckBox { spacing: 13px; }");
            }
            m_qCheckBox4[index]->setLayoutDirection(Qt::RightToLeft);
            m_qGLytChkBox4->addWidget(m_qCheckBox4[index], row, col);
        }
    }





    m_qGBoxAllocMem4->setLayout(m_qGLytChkBox4);
    m_qGLytChkBox4->addItem(slaveSpacer, 5, 5);
}
