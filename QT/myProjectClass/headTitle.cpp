#include <headTitle.h>

headTitle::headTitle(const QString& qStrForTitle1, const QString& qStrForTitle2)
{
    QLabel* qLblTitle1 = new QLabel(qStrForTitle1);
    QLabel* qLblTitle2 = new QLabel(qStrForTitle2);
    QVBoxLayout* qVlytHead = new QVBoxLayout;
    setLayout(qVlytHead);
    qVlytHead->addWidget(qLblTitle1);
    qVlytHead->addWidget(qLblTitle2);
    setStyleSheet("background: qlineargradient( x1:0 y1:0, x2:1 y2:0, stop:0 darkcyan, stop:1 cyan);");
    //m_qWgdHeadColor->setStyleSheet("* {color: qlineargradient(spread:pad, x1:0 y1:0,x2:1 y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));""background: qlineargradient( x1:0 y1:0, x2:1 y2:0, stop:0 darkcyan, stop:1 cyan);}");
    qLblTitle1->setStyleSheet("color: black; font: bold 16px;");
    qLblTitle2->setStyleSheet("color: black; font: bold 11px;");
    this->show();

}
