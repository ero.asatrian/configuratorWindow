#include <radioButtonBox2.h>

radioButtonBox2::radioButtonBox2(const QString& qStrForGBox, const QString& qStrForLabel, const QStringList& qStrLstForRadBut, int size, int spacerSizeBox2)
{
    m_qStrLstForRadBut = qStrLstForRadBut;
    m_qRadioB = new QRadioButton*[size]; 
    m_qGBoxDataWidth = new QGroupBox(qStrForGBox);
    QSpacerItem *configSpacer = new QSpacerItem(spacerSizeBox2,
0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	m_qLblDataWidth = new QLabel(qStrForLabel);
    m_qHBlytGBoxDataWidth = new QHBoxLayout;
    m_qHBlytGBoxDataWidth->addWidget(m_qLblDataWidth);
    for(int i = 0; i < size; ++i){
        m_qRadioB[i] = new QRadioButton(m_qStrLstForRadBut[i]);
        m_qHBlytGBoxDataWidth->addWidget(m_qRadioB[i]);
    }
    m_qGBoxDataWidth->setLayout(m_qHBlytGBoxDataWidth); 
	m_qHBlytGBoxDataWidth->addItem(configSpacer);
}
