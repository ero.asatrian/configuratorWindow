#include "labelComboBox3.h"

labelComboBox3::labelComboBox3()
{  
    m_qGBoxAddresConfig = new QGroupBox("Address Configuration");
    QStringList qStrLstNumOfAddr1 = {"32", "28", "24", "20", "16", "12"};
    QString qStrForG3Lbl1 = "Number of address bits driven by master:";
    m_labelWidthComboBox3_1 = new labelComboBox(qStrForG3Lbl1, qStrLstNumOfAddr1, 20);
    m_qVBLtB3 = new QVBoxLayout();
    m_qVBLtB3->addLayout(m_labelWidthComboBox3_1->m_qHBLyt);
    QStringList qStrLstNumOfAddr2 = {"Upper 4 master address bits do not appear in slave address", "[31:28]", "[27:24](Ignored if master address width >= 32 bits)",
    "[23:20](Ignored if master address width >= 28 bits)", "[19:16](Ignored if master address width >= 24 bits)", "[15:12](Ignored if master address width >= 20 bits", "[11:8](Ignored if master address width >= 16  bits"};
    QString qStrForG3Lbl2 = "Position in slave address of upper 4 bits of master address:";
    m_labelWidthComboBox3_2 = new labelComboBox(qStrForG3Lbl2, qStrLstNumOfAddr2, 20);
    m_qVBLtB3->addLayout(m_labelWidthComboBox3_2->m_qHBLyt);

    QStringList qStrLstNumOfAddr3 = {"Not in use", "Indirect address sourced form IADDR input port", "Indirect address sourced from register(s) in slot 0 space", "Indirect address sourced from register(s) in slot 1 space", "Indirect address sourced from register(s) in slot 2 space", "Indirect address sourced from register(s) in slot 3 space","Indirect address sourced from register(s) in slot 4 space", "Indirect address sourced from register(s) in slot 5 space", "Indirect address sourced from register(s) in slot 6 space","Indirect address sourced from register(s) in slot 7 space"};
    QString qStrForG3Lbl3 = "Indirect Addressing"; 
    m_labelWidthComboBox3_3 = new labelComboBox(qStrForG3Lbl3, qStrLstNumOfAddr3, 20);
    m_qVBLtB3->addLayout(m_labelWidthComboBox3_3->m_qHBLyt);
    m_qGBoxAddresConfig->setLayout(m_qVBLtB3);
}


