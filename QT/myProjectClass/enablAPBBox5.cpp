#include "enablAPBBox5.h"
#include <iostream>

enablAPBBox5::enablAPBBox5()
{
    int arrSize = 16;
    m_arrChekedChBox5 = new bool[arrSize];
    m_qGBoxEnabledAPB5 = new QGroupBox("Enabled APB Slave Slots");
    QSpacerItem *slaveSpacer = new QSpacerItem(1000, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_qGLytChkBox5 = new QGridLayout;
    m_qCheckBox5 = new QCheckBox*[arrSize];
    for (int row = 0; row < 4; ++row){
        for (int col = 0; col < 4; ++col){
            int index = row * 4 + col;
            m_qCheckBox5[index] = new QCheckBox(QString("Slot %1").arg(index));
            if (index < 10){
                m_qCheckBox5[index]->setStyleSheet("QCheckBox { spacing: 13px; }");
            }
            m_qCheckBox5[index]->setLayoutDirection(Qt::RightToLeft);
            m_qGLytChkBox5->addWidget(m_qCheckBox5[index], row, col);
        }
    }
    m_qGBoxEnabledAPB5->setLayout(m_qGLytChkBox5);
    //m_qCheckBox5[2]->setEnabled(false);
    m_qGLytChkBox5->addItem(slaveSpacer, 5, 5);
}

