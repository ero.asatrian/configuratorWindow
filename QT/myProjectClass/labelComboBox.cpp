#include "labelComboBox.h"
#include <QDebug>

labelComboBox::labelComboBox( const QString& qStringforLabel,const QStringList& qStrComboBox, int spacerSize, QString qStrForGName)
{ 
    
    QSpacerItem* spacerLblCombo = new QSpacerItem(spacerSize, 0, QSizePolicy::Minimum, QSizePolicy::Minimum);
    QLabel* qLabelforComboBox = new QLabel(qStringforLabel); 
    m_qComboBox = new QComboBox;
    m_qHBLyt = new QHBoxLayout;
     if (qStrForGName != "" ){
        m_qGBox = new QGroupBox(qStrForGName);
        m_qGBox->setLayout(m_qHBLyt);
    }   
    m_qComboBox->addItems(qStrComboBox);
    m_qHBLyt->addWidget(qLabelforComboBox);
    m_qHBLyt->addWidget(m_qComboBox);
    m_qHBLyt->addItem(spacerLblCombo);
}
/*
labelComboBox::labelComboBox()
{  
    m_qGBox = new QGroupBox("Global Configuration");
    m_spacerLblCombo = new QSpacerItem(1700, 0, QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_qLabelforComboBox = new QLabel("Reset Type"); 
    QStringList qStrComboBox;
    qStrComboBox.append("Asynchronous");
    qStrComboBox.append("Synchronous");
    m_qComboBox = new QComboBox;
    m_qComboBox->addItems(qStrComboBox);
    m_qHBLyt = new QHBoxLayout;
    m_qHBLyt->addWidget(m_qLabelforComboBox);
    m_qHBLyt->addWidget(m_qComboBox);
    m_qHBLyt->addItem(m_spacerLblCombo); 
    m_qGBox->setLayout(m_qHBLyt);
    //setLayout(m_qHBLyt);
}
*/
