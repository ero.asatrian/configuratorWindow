#include <QtWidgets>
#include <QApplication>
#include <iostream>
#include <string>
#include "configMainWindow.h"
#include "labelComboBox.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setStyle(QStyleFactory::create("Windows"));
    configMainWindow window;
    window.show();    
    return app.exec();
}
