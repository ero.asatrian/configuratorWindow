#ifndef __CONFIG_MAIN_CLASS
#define __CONFIG_MAIN_CLASS

#include <QApplication>
#include <QMainWindow>
#include <QtWidgets>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QString>
#include <QLabel>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <iostream>
#include "labelComboBox.h"
#include "radioButtonBox2.h"
#include "labelComboBox3.h"
#include "allocateMemBox4.h"
#include "enablAPBBox5.h"
#include "partAcceptSetup.h"
#include "headTitle.h"

class configMainWindow:public QMainWindow{
    Q_OBJECT
    private:
        QWidget *m_widgetCentral;
        QWidget *m_widgetTab1;
        QTabWidget *m_tabWidget;
        QVBoxLayout *m_VLayoutMain;
        QVBoxLayout *m_VLayouTab1; 
        headTitle *m_headTitle; // Head title part
        labelComboBox *m_labelWidthComboBox; // First GBox for  Global configuration
        radioButtonBox2 *m_radioButtonLblB2; // Second GBox for data width configuration
        labelComboBox3 *m_labelComboBox3; // Third GBox for address configuration
        allocateMemBox4 *m_allocMemBox4; // Fourth GBox for allocate memery space to combined region slave
        enablAPBBox5 *m_enablAPBBox5; // Fifth GBox for enabled APB Slave Slots
        labelComboBox *m_testBanch; // Test banch select permisions
        partAcceptSetup *m_partAccept; // Combine of buttons: Help, Ok and cancel.
    public:
        QJsonObject qJsonObj;
        QString m_fileName;
        configMainWindow();
        configMainWindow(QWidget*, QTabWidget*, QVBoxLayout*, QVBoxLayout*, QString, int, int);
        QString readJSONQStr(const QString&, const  QString&);
        bool* readJSONInt(const QString&, const QString&, bool*, int);
        void setupWindowNameSize(QString, int, int);
        void setupTabInWindow(const QString);
        void addHeadTitle();
        void addTabWidget();   
        void addGlobConfigGBox1();
        void addDataConfigGBox2();
        void addAddresConfigGBox3();
        void addAllocMemGBox4();
        void addEnabledAPBGBox5();
        void addTestBanch();
        void addPartAccept();
        void saveJsonQGBox1();
        void saveJsonQGBox2();
        void saveJsonQGBox3();
        void saveJsonQGBox4();
        void saveJsonQGBox5();
        void saveJsonTestBanch();
};

#endif
