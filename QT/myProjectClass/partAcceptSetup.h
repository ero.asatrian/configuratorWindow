#ifndef __PARTACCEPT_
#define __PARTACCEPT_

#include <QPushButton>
#include <QHBoxLayout>

class partAcceptSetup:public QWidget{
    Q_OBJECT
    private:
        QPushButton *m_qPushBHelp;
        QPushButton *m_qPushBCancel;
    public:
        QPushButton *m_qPushBOK;
        QHBoxLayout *m_qHBLytAccept;
        partAcceptSetup();
};
#endif


