#ifndef __LABEL_COMBOBOX3_
#define __LABEL_COMBOBOX3_

#include <QVBoxLayout>
#include <QString>
#include <QLabel>
#include <QComboBox>
#include <QGroupBox>
#include "labelComboBox.h"

class labelComboBox3:public QWidget{
    Q_OBJECT
       public: 
       labelComboBox * m_labelWidthComboBox3_1;
       labelComboBox * m_labelWidthComboBox3_2;
       labelComboBox * m_labelWidthComboBox3_3;
        QGroupBox *m_qGBoxAddresConfig;
        QVBoxLayout *m_qVBLtB3;
        labelComboBox3();
};

#endif

